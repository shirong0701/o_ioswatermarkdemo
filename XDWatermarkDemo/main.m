//
//  main.m
//  WaterMarkDemo
//
//  Created by Billy on 13-3-31.
//  Copyright (c) 2013 Billy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DXAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DXAppDelegate class]));
    }
}
