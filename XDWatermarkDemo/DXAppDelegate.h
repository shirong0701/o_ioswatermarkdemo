//
//  DXAppDelegate.h
//  WaterMarkDemo
//
//  Created by Billy on 13-3-31.
//  Copyright (c) 2013 Billy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DXViewController;

@interface DXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) DXViewController *viewController;

@end
